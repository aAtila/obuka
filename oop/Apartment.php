<?php

namespace App;

class Apartment implements AdInterface
	{
		use AdTrait;

	public function getM2(): int
		{
		return $this->m2 + 100;
		}

	public function setM2(int $someM2): self
		{
		$this->m2 = $someM2 * 15;

		return $this;
		}
	}