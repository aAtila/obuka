<?php

namespace App;

class House implements AdInterface
	{

	use AdTrait;

	/**
	 * @var int
	 */
	private static $yard = 88;

	public static function getYard(): int
		{
		return self::$yard;
		}

	public static function setYard(int $someYard)
		{
		self::$yard = $someYard;
		}
	}