<?php

  require_once __DIR__.'/AdInterface.php';
  require_once __DIR__.'/AdTrait.php';
  require_once __DIR__.'/Apartment.php';
  require_once __DIR__.'/House.php';

  use \App\Apartment;
  use \App\House;

  $apartment = new Apartment(33250, 77);
  $apartment->setM2(83)
	        ->setM2(77)
	        ->setM2(177);

  $house = new House(55000, 160);

  echo 'Apartment m2: ' . $apartment->getM2() . ' | ' . 'Yard: ' . $house->getYard() . 'm2';
  echo '<hr>';
  var_dump($apartment);
  echo '<hr>';
  var_dump($house);
  echo '<hr>';
  echo 'Yard is a static property. It has the value: ' . House::getYard();
  echo '<hr>';
  House::setYard(77);
  echo 'Calling yard\'s value through the obj: ' . $house->getYard();