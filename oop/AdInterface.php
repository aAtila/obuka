<?php

namespace App;

interface AdInterface
	{
	public function getPrice();

	public function getM2();

	public function setPrice(int $price);

	public function setM2(int $m2);
	}