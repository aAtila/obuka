<?php

namespace App;

trait AdTrait
	{
	/**
	 * @var int
	 */
	protected $price;

	/**
	 * @var int
	 */
	protected $m2;

	public function __construct(int $propertyPrice, int $propertyM2)
		{
		$this->price = $propertyPrice;
		$this->m2    = $propertyM2;
		}

	public function getPrice(): int
		{
		return $this->price;
		}

	public function getM2(): int
		{
		return $this->m2;
		}

	public function setPrice(int $somePrice): self
		{
		$this->price = $somePrice;

		return $this;
		}

	public function setM2(int $someM2): self
		{
		$this->m2 = $someM2;

		return $this;
		}
	}